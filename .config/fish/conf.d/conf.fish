function vim
	command nvim $argv
end
# Startup stuff
setxkbmap -option caps:super
xset r rate 300 50
feh --bg-fill ~/extra/.wallpaper/animals.jpg  

alias py="python3"
alias :q="exit"
alias godnatt="poweroff"
alias y1="cd ~/uisfiles/year1"
alias lego="cd ~/uisfiles/year1/ing100/Lego"
alias gaa="git add --all"
alias gc="git commit"
alias cpp="cd ~/programering/cpp"
alias i3conf="vim ~/.config/i3/config"
alias f="cd ~/.config/fish"
